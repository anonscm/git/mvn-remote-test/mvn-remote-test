package org.evolvis.maven.plugins.remotetesting.test.blackbox

import org.evolvis.maven.plugins.remotetesting.projectbuilder.ProjectBuilder
import org.evolvis.maven.plugins.remotetesting.util.ConfigurationAndObjectsFactory

/**
 * this test class "simulates" the use of the remote-testing-plugin using the example-project module
 * pom is generated "on the fly"
 */
abstract class AbstractBlackBoxTest extends GroovyTestCase {

    ConfigurationAndObjectsFactory configAndObjects = new ConfigurationAndObjectsFactory()
    ProjectBuilder projectBuilder = new ProjectBuilder()

    /**
     * tests the clean goal
     */
    void testClean() {
        // put waste on remote system
        configAndObjects.wasteSystem()

        // make SURE remote system is wasted
        assertTrue(configAndObjects.remoteMachine.executeCommand("ls /tmp").contains("remoteTestingFakeProject"))

        // execution
        configAndObjects.buildMachine.executeCommand("mvn -f ${configAndObjects.workingDirectory}/pom.xml -P local clean")

        // check if it cleaned the remote system
        assertFalse(configAndObjects.remoteMachine.executeCommand("ls /tmp").contains("remoteTestingFakeProject"))
    }

    /**
     * tests the remotetesting-goal
     */
    void testRemoteTesting() {
        // try and remove target folder
        configAndObjects.buildMachine.executeCommand("rm -r -f ${configAndObjects.workingDirectory}/target")

        // make sure it does not exist
        def output = configAndObjects.buildMachine.executeCommand("ls -la ${configAndObjects.workingDirectory}")
        assertFalse(output.contains("target"))

        // execution
        configAndObjects.buildMachine.executeCommand("mvn -f ${configAndObjects.workingDirectory}/pom.xml -P local test")

        // check if both tests were executed (means remote test results were fetched)
        def content = configAndObjects.buildMachine.executeCommand("ls -la ${configAndObjects.workingDirectory}/target/surefire-reports")
        assertTrue(content.contains("Local"))
        assertTrue(content.contains("Remote"))
    }
}
