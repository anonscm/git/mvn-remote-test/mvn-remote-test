package org.evolvis.maven.plugins.remotetesting.test.blackbox

/**
 * this test class "simulates" the use of the remote-testing-plugin using the example-project module
 * pom is generated "on the fly"
 */
class BlackBoxTestUsingXvfb_IT extends AbstractBlackBoxTest {

    void setUp() {
        projectBuilder.buildBlackboxProject(true)
    }
}
