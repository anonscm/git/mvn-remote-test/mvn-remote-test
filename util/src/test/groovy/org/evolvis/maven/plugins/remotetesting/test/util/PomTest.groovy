package org.evolvis.maven.plugins.remotetesting.test.util

import org.evolvis.maven.plugins.remotetesting.util.ConfigurationAndObjectsFactory
import org.evolvis.maven.plugins.remotetesting.util.Pom

/**
 * this test checks the functionality of the Pom-class
 */
class PomTest extends GroovyTestCase {

    ConfigurationAndObjectsFactory configAndObjects = new ConfigurationAndObjectsFactory()

    Pom pom

    void setUp() {
        pom = new Pom("src/test/resources/pom.xml")
    }

    void tearDown() {
        configAndObjects.buildMachine.tryDeleteDir(configAndObjects.workingDirectory)
    }

    /**
     * tests whether the profiles are extracted from the pom correctly
     */
    void testGetProfiles() {
        def profiles = pom.configuredProfiles()
        assertEquals(1, profiles.size())
        assertTrue(profiles.contains("remote"))
    }
}
