package org.evolvis.maven.plugins.remotetesting.test.remotemachine

import org.evolvis.maven.plugins.remotetesting.util.ConfigurationAndObjectsFactory

/**
 * this class tests the RemoteMachine class
 */
abstract class AbstractRemoteMachineTest extends GroovyTestCase {

    def fileName = "archive.tar"
    def sourceFilePath = "src/test/resources/"
    def targetFolderPath = "target/remoteTesting"

    ConfigurationAndObjectsFactory configAndObjects = new ConfigurationAndObjectsFactory()

    /**
     * tests remote execution of a command via ssh
     */
    void testSSH() {
        // execute command
        def output = configAndObjects.remoteMachine.executeCommand("ls")

        // check whether the output seems to be valid
        assertTrue(output.length() > 0)
        //assertTrue(output.contains("ls "))
    }

    /**
     * tests copying a file to a remote machine using scp
     */
    void testCopyFileToRemoteMachine() {
        configAndObjects.remoteMachine.tryDeleteFile("/tmp/${fileName}")

        // file shall not exist
        def lsBefore = configAndObjects.remoteMachine.executeCommand("ls /tmp")
        println(lsBefore)
        assertFalse("remove /tmp/${fileName} from remote machine!", lsBefore.contains(fileName))

        // copy it
        configAndObjects.remoteMachine.storeFile(sourceFilePath + fileName, "/tmp")

        // make sure it exists now
        String lsAfter = configAndObjects.remoteMachine.executeCommand("ls /tmp")
        assertTrue(lsAfter.contains(fileName))
    }

    /**
     * tests copying a file from a remote machine using scp
     */
    void testCopyFileFromRemoteMachine() {
        // make sure the file doesnt exist yet
        def existsBefore = new File("${targetFolderPath}/${fileName}").exists()
        assertFalse("clean up before running this test!", existsBefore)

        configAndObjects.buildMachine.tryMakeDir(targetFolderPath)

        // copy something to remote machine to make sure its there
        configAndObjects.remoteMachine.storeFile(sourceFilePath + fileName, "/tmp")

        // copy back
        configAndObjects.remoteMachine.deliverFile("/tmp/${fileName}", targetFolderPath)

        // make sure the file now exists
        def existsAfter = new File("${targetFolderPath}/${fileName}").exists()
        assertTrue(existsAfter)
    }

    void tearDown() {
        // clean up
        configAndObjects.remoteMachine.tryDeleteFile("/tmp/${fileName}")
        configAndObjects.buildMachine.tryDeleteFile("${targetFolderPath}/${fileName}")
    }
}
