package org.evolvis.maven.plugins.remotetesting.test.remotemachine

import org.evolvis.maven.plugins.remotetesting.util.RemoteMachine

/**
 * this class tests the RemoteMachine class
 */
class RemoteMachineTestUsingKeyFile_IT extends AbstractRemoteMachineTest {

    RemoteMachine passwordEnabledRemoteMachine
    RemoteMachine keyEnabledRemoteMachine

    void setUp() {
        passwordEnabledRemoteMachine = configAndObjects.remoteMachine
        def username = passwordEnabledRemoteMachine.username
        def host = passwordEnabledRemoteMachine.host
        keyEnabledRemoteMachine = new RemoteMachine(host, username, null, "src/test/resources/id_rsa", null)
        // just to make sure the tests use the correct machine
        configAndObjects.remoteMachine = keyEnabledRemoteMachine
        storeKey()
    }


    void tearDown() {
        removeKeys()
    }

    void testDistributedKey() {
        def output = keyEnabledRemoteMachine.executeCommand("ls")
        assertNotNull(output)
        assertTrue(output.length() > 1)
    }

    void testMissingKey() {
        removeKeys()
        Boolean caught = false
        try {
            keyEnabledRemoteMachine.executeCommand("ls")
        } catch (ex) {
            assertTrue(ex.message.contains("Auth cancel"))
            caught = true
        }
        assertTrue(caught)
    }

    private void storeKey() {
        passwordEnabledRemoteMachine.tryDeleteFile("/tmp/id_rsa.pub")
        passwordEnabledRemoteMachine.storeFile("src/test/resources/id_rsa.pub", "/tmp")
        passwordEnabledRemoteMachine.tryMakeDir(".ssh")
        passwordEnabledRemoteMachine.executeCommand("cat /tmp/id_rsa.pub >> .ssh/authorized_keys")
    }

    private void removeKeys() {
        passwordEnabledRemoteMachine.tryDeleteFile(".ssh/authorized_keys")
    }
}
