package org.evolvis.maven.plugins.remotetesting.test.util


import org.evolvis.maven.plugins.remotetesting.util.BuildMachine

/**
 * tests for class BuildMachine (todo: add more :))
 */
class BuildMachineTest extends GroovyTestCase {

    def targetFolderPath = "target/remoteTesting"
    BuildMachine buildMachine = new BuildMachine()

    void setUp() {
        buildMachine.tryDeleteDir(targetFolderPath)
        buildMachine.tryMakeDir(targetFolderPath)
    }

    void tearDown() {
        buildMachine.tryDeleteDir(targetFolderPath)
    }

    /**
     * tests copying a file
     */
    void testCopyFile() {
        // shall not exist
        def existsBefore = new File(targetFolderPath + "/foo.bar").exists()
        assertFalse("clean up before running this test!", existsBefore)

        // copy
        buildMachine.copyFile("src/test/resources/archive.tar", targetFolderPath + "/foo.bar")

        // shall  exist
        def existsAfter = new File(targetFolderPath + "/foo.bar").exists()
        assertTrue(existsAfter)
    }

    /**
     * tests the creation of an archive of the current folder
     */
    void testFolderCompression() {
        // shall not exist
        def existsBefore = new File(targetFolderPath + "/project.zip").exists()
        assertFalse("clean up before running this test!", existsBefore)

        // create
        buildMachine.createZip(".", targetFolderPath, "project.zip")

        // shall exist
        def archive = new File(targetFolderPath + "/project.zip")
        assertTrue(archive.exists())

        // clean up
        archive.delete()
    }

    /**
     * tests whether a copy operation is successful (at least if anything is copied)
     */
    void testCopyFolder() {
        // copy folder
        buildMachine.copyFolder("", targetFolderPath)

        // assert it exists, is dir and not empty
        def targetFolder = new File(targetFolderPath)
        assertTrue(targetFolder.exists())
        assertTrue(targetFolder.isDirectory())
        assertTrue(targetFolder.list().length > 1)
    }
}
