package org.evolvis.maven.plugins.remotetesting.util

/**
 * this class provides helper operations on the pom.xml file
 */
class Pom {

    /**
     * the xml content of the pom
     */
    Node xml

    /**
     * the file instance of the pom
     */
    File pomFile

    /**
     * the absolut path of the pom
     */
    def absolutePathToPom

    /**
     * the real filename of the pom
     */
    def pomFileName

    def Pom(String pom = "pom.xml") {

        pomFile = new File(pom)

        if (!pomFile.exists()) {
            throw new Exception("pomfile specified (${pom}, ${pomFile.absolutePath}) does not exist")
        }

        // determine pom path and name
        def pathAndName = pomFile.absolutePath
        pomFileName = pathAndName.substring(pathAndName.lastIndexOf("/") + 1)
        absolutePathToPom = pathAndName.substring(0, pathAndName.lastIndexOf("/"))

        // workaround for strange parse() behaviour
        String text = pomFile.text
        xml = new XmlParser().parseText(text)
    }

    /**
     * returns a list of all profiles configured in the pom
     * @param path (optional)
     * @return
     */
    ArrayList configuredProfiles() {
        def profiles = new ArrayList<String>()
        xml.get("profiles").getAt("profile").each {
            profiles.add(it.get("id")[0].text())
        }
        profiles
    }
}
