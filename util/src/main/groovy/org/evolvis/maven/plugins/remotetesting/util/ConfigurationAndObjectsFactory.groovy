package org.evolvis.maven.plugins.remotetesting.util

/**
 * this is a helper class which takes extracts some config values, instatiates some helpful objects needed
 * by some tests
 */
class ConfigurationAndObjectsFactory {

    def config

    BuildMachine buildMachine
    RemoteMachine remoteMachine

    String pluginVersion
    String workingDirectory

    ConfigurationAndObjectsFactory() {
        config = new ConfigSlurper().parse(this.class.getResource("/mvn-remote-test.config"))

        buildMachine = new BuildMachine()
        remoteMachine = new RemoteMachine(config.test.remotemachine.host, config.test.remotemachine.user,
                config.test.remotemachine.pass, null, null)

        pluginVersion = config.test.pluginVersion
        workingDirectory = config.test.workingDir
    }

    /**
     * makes sure the directory "/tmp/remoteTesting" exists and contains a file named bar
     */
    def wasteSystem() {
        remoteMachine.tryMakeDir("/tmp/remoteTesting")
        remoteMachine.executeCommand("echo foo >> /tmp/remoteTesting/bar")
    }
}
