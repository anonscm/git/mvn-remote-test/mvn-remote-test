package org.evolvis.maven.plugins.remotetesting.util

import org.apache.maven.plugin.logging.SystemStreamLog

/**
 * provides convenience for operations on the local/build machine
 */
class BuildMachine {

    def log
    AntBuilder ant

    BuildMachine(log = new SystemStreamLog()) {
        this.log = log
        ant = new AntBuilder()
    }

    /**
     * just a helper that logs messages surrounded by two blank lines (on info)
     * @param message
     */
    def logLoud(message) {
        log.info("")
        log.info("[remote-testing-plugin] ${message}")
        log.info("")
    }

    /**
     * execution of commands, throws an exception if the process returns with errors
     * @param command , the command to execute
     * @param logOutput , optional (default=true), log output to console
     * @return returns the process output
     */
    def executeCommand(String command, Boolean logOutput = true) {
        def process = command.execute()
        process.waitFor()
        def output = process.text
        def exitValue = process.exitValue()
        if (logOutput) {
            logLoud("executed: ${command}, output: ${output}, exitValue: ${exitValue}")
        }
        if (exitValue != 0) {
            throw new Exception("process executed with error. check output: ${output}")
        }
        output
    }

    /**
     * method to create zip archive
     * @param sourcedir
     * @param targetDir
     * @param filename
     */
    def createZip(sourcedir, targetDir, filename) {
        ant.mkdir(dir: targetDir)
        ant.zip(basedir: sourcedir, destfile: "${targetDir}/${filename}")
    }

    /**
     * method to create tar archive
     * @param sourcedir
     * @param targetDir
     * @param filename
     */
    def createTar(sourcedir, targetDir, filename) {
        ant.mkdir(dir: targetDir)
        def tarCommand = "tar -cf ${targetDir}/${filename} ${sourcedir}"
        executeCommand(tarCommand)
    }

    /**
     * method to extract an archive
     * @param pathToArchive
     * @param targetDir
     */
    def extractZip(pathToArchive, targetDir) {
        ant.mkdir(dir: targetDir)
        ant.unzip(src: pathToArchive, dest: targetDir, overwrite: true)
    }

    /**
     * method to extract an archive
     * @param pathToArchive
     * @param targetDir
     */
    def extractTar(pathToArchive, targetDir) {
        ant.mkdir(dir: targetDir)
        def untarCommand = "tar xf ${pathToArchive} -C ${targetDir}"
        executeCommand(untarCommand)
    }

    /**
     * this method copies a complete folder with all subfolders
     * @param sourcedir
     * @param targetdir
     * @return
     */
    def copyFolder(sourcedir, targetdir) {
        ant.mkdir(dir: targetdir)
        ant.copy(todir: targetdir) {
            fileset(dir: sourcedir)
        }
    }

    /**
     * this method copies a single file
     * @param sourceFile
     * @param targetFile
     */
    def copyFile(sourceFile, targetFile) {
        executeCommand("cp ${sourceFile} ${targetFile}")
    }

    /**
     * simple helper that attempts to delete a directory and ignores exceptions
     * @param dir
     */
    def tryDeleteDir(dir) {
        try {
            executeCommand("rm -R -f ${dir}")
        } catch (ex) {}
    }

    /**
     * simple helper that attempts to delete a file and ignores exceptions
     * @param dir
     */
    def tryDeleteFile(file) {
        try {
            executeCommand("rm -f ${file}")
        } catch (ex) {}
    }

    /**
     * simple helper that attempts to create a directory and ignores exceptions
     * @param dir
     */
    def tryMakeDir(dir) {
        try {
            ant.mkdir(dir: dir)
        } catch (ex) {}
    }
}