package org.evolvis.maven.plugins.remotetesting.util

import org.apache.maven.plugin.logging.SystemStreamLog

/**
 * This class handles all remoteMachine with the remote machine (ssh, scp, ...)
 */
class RemoteMachine {

    def log
    String host
    String username
    String password
    String keyFile
    String passphrase
    AntBuilder ant = new AntBuilder()

    RemoteMachine(host, username, password, keyFile, passphrase, log = new SystemStreamLog()) {
        this.log = log
        this.host = host
        this.username = username
        this.password = password
        if (keyFile == null) {
            this.keyFile = "/home/${username}/.ssh/id_rsa"
        } else {
            this.keyFile = keyFile
        }
        if (this.passphrase == null) {
            this.passphrase = " "
        } else {
            this.passphrase = passphrase
        }
    }

    private Boolean useKeyFile() {
        return password == null || password.length() < 1
    }

    /**
     * executes a command on the remote machine
     * @param cmd
     */
    def executeCommand(cmd) {
        // new AntBuilder-instance as workaround for poor/buggy ant-implementation (result will not get overwritten)
        def newAntBuilder = new AntBuilder()

        log.info("[remote-testing-plugin] executing: ${cmd} on ${host}")
        if (useKeyFile()) {
            newAntBuilder.sshexec(host: host, username: username, outputproperty: "result", keyfile: keyFile,
                    passphrase: passphrase, command: cmd, trust: true)
        } else {
            newAntBuilder.sshexec(host: host, username: username, password: password, outputproperty: "result",
                    command: cmd, trust: true)
        }
        // return output
        newAntBuilder.getProject().getProperty("result")
    }

    /**
     * stores a file on the remote machine
     * @param sourceFile
     * @param targetPath
     */
    def storeFile(sourceFile, targetPath) {
        // create folder if it does not exist
        try {
            ant.sshexec(host: host, username: username, password: password, command: "mkdir ${targetPath}", trust: true)
        } catch (ex) {
            log.warn("couldnt create folder. maybe it is simply already existing and this is nothing to worry about."
                    + " simply invoke clean.")
        }
        // copy file
        if (useKeyFile()) {
            ant.scp(file: sourceFile, todir: "${username}@${host}:${targetPath}", keyfile: keyFile,
                    passphrase: passphrase, trust: true)
        } else {
            ant.scp(file: sourceFile, todir: "${username}:${password}@${host}:${targetPath}", trust: true)
        }
    }

    /**
     * copies a specified file from the remote machine
     * @param sourceFile
     * @param targetPath
     */
    def deliverFile(sourceFile, targetPath) {
        // copy file
        if (useKeyFile()) {
            ant.scp(file: "${username}@${host}:${sourceFile}", keyfile: keyFile, passphrase: passphrase,
                    todir: targetPath, trust: true)
        } else {
            ant.scp(file: "${username}:${password}@${host}:${sourceFile}", todir: targetPath, trust: true)
        }
    }

    /**
     * a little helper which does not fail if the mkdir-command fails
     * @param dir
     * @return
     */
    def tryMakeDir(dir) {
        try {
            executeCommand("mkdir ${dir}")
        } catch (ex) {}
    }

    /**
     * a little helper that attempts to delete a file and doesent cause exceptions if the command fails
     * @param file
     */
    def tryDeleteFile(file) {
        try {
            executeCommand("rm ${file}")
        } catch (ex) {}
    }

    /**
     * a little helper that attempts to delete a directory and doesent cause exceptions if the command fails
     * @param dir
     */
    def tryDeleteDir(dir) {
        try {
            executeCommand("rm -r -f ${dir}")
        } catch (ex) {}
    }
}
