package org.evolvis.maven.plugins.remotetesting.test.projectbuilder

import org.evolvis.maven.plugins.remotetesting.projectbuilder.ProjectBuilder
import org.evolvis.maven.plugins.remotetesting.util.ConfigurationAndObjectsFactory

/**
 * test for the project build
 */
class ProjectBuilderTest extends GroovyTestCase {

    ConfigurationAndObjectsFactory configAndObjects = new ConfigurationAndObjectsFactory()
    ProjectBuilder projectBuilder = new ProjectBuilder()

    void setUp() {
        configAndObjects.buildMachine.tryDeleteDir(configAndObjects.workingDirectory)
    }

    void tearDown() {
        configAndObjects.buildMachine.tryDeleteDir(configAndObjects.workingDirectory)
    }

    void testProjectBuilder() {
        assertFalse("clean up!", new File(configAndObjects.workingDirectory).exists())
        projectBuilder.buildBlackboxProject()
        assertTrue(new File(configAndObjects.workingDirectory + "/pom.xml").exists())
        assertTrue(new File(configAndObjects.workingDirectory + "/src/test/java/Local.java").exists())
        assertTrue(new File(configAndObjects.workingDirectory + "/src/test/java/Remote.java").exists())
    }
}
