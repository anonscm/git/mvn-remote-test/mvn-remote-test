package org.evolvis.maven.plugins.remotetesting.test.projectbuilder

import org.evolvis.maven.plugins.remotetesting.projectbuilder.PomCreator
import org.evolvis.maven.plugins.remotetesting.util.ConfigurationAndObjectsFactory

/**
 * tests for the test testutil "PomCreator"
 */
class PomCreatorTest extends GroovyTestCase {

    PomCreator pomCreator
    ConfigurationAndObjectsFactory configAndObjects = new ConfigurationAndObjectsFactory()

    void setUp() {
        pomCreator = new PomCreator()
        configAndObjects.buildMachine.tryDeleteDir(configAndObjects.workingDirectory)
    }

    void tearDown() {
        configAndObjects.buildMachine.tryDeleteDir(configAndObjects.workingDirectory)
    }

    /**
     * causes a minimal valid pom to be created and checks the context
     */
    void testMinimalValidPom() {
        File pomFile = new File("${configAndObjects.workingDirectory}/pom.xml")
        assertFalse("please remove ${configAndObjects.workingDirectory} before executing this test", pomFile.exists())
        pomCreator.createPom()
        pomFile = new File("${configAndObjects.workingDirectory}/pom.xml")
        assertTrue(pomFile.exists())
    }

    /**
     * test the method with additional parameter
     */
    void testWithAdditionalParameter() {
        pomCreator.createPom(true, "<foo>bar</foo>")
        assertTrue(new File("${configAndObjects.workingDirectory}/pom.xml").text.contains("<foo>bar</foo>"))
    }
}
