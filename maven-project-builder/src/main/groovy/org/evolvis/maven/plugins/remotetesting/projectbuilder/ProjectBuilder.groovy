package org.evolvis.maven.plugins.remotetesting.projectbuilder

import org.evolvis.maven.plugins.remotetesting.util.ConfigurationAndObjectsFactory

/**
 * Used to build maven projects
 */
class ProjectBuilder {

    ConfigurationAndObjectsFactory configAndObjects = new ConfigurationAndObjectsFactory()

    /**
     * removes the fakeProject-directory
     */
    private def cleanUp() {
        configAndObjects.buildMachine.tryDeleteDir(configAndObjects.workingDirectory)
        configAndObjects.buildMachine.tryMakeDir("target")
        configAndObjects.buildMachine.tryMakeDir(configAndObjects.workingDirectory)
    }

    /**
     * puts test classes under src/test/java
     */
    private def copyTestClasses() {
        configAndObjects.buildMachine.tryMakeDir("${configAndObjects.workingDirectory}/src")
        configAndObjects.buildMachine.tryMakeDir("${configAndObjects.workingDirectory}/src/main")
        configAndObjects.buildMachine.tryMakeDir("${configAndObjects.workingDirectory}/src/main/java")
        configAndObjects.buildMachine.tryMakeDir("${configAndObjects.workingDirectory}/src/test")
        configAndObjects.buildMachine.tryMakeDir("${configAndObjects.workingDirectory}/src/test/java")
        configAndObjects.buildMachine.copyFile("src/test/resources/local.exampleclass",
                "${configAndObjects.workingDirectory}/src/test/java/Local.java")
        configAndObjects.buildMachine.copyFile("src/test/resources/remote.exampleclass",
                "${configAndObjects.workingDirectory}/src/test/java/Remote.java")

    }

    /**
     * lets build a valid and usable pom file
     *
     * @return
     */
    private def buildBlackBoxPom(Boolean useXvfb) {
        PomCreator pomCreator = new PomCreator()

        // specify packaging
        pomCreator.pomPackaging = "jar"

        // set executions
        pomCreator.rtPluginExecutions = "<execution><id>remote testing</id><goals><goal>clean</goal>" +
                "<goal>test</goal></goals></execution>"

        // initialize profiles for local and remote tests
        String rtExtraConfiguration = "<pomfile>${configAndObjects.workingDirectory}/pom.xml</pomfile>"
        if (useXvfb) {
            rtExtraConfiguration += "<display>xvfb:6</display>"
        }
        def localProfile = "<profile><id>local</id><dependencies>" +
                pomCreator.buildSWTDependencyEntry(true) +
                "</dependencies><build><plugins>" +
                pomCreator.buildRTPluginEntry(rtExtraConfiguration) +
                pomCreator.buildSureFirePluginEntry("<include>**/*Local.java</include>") +
                "</plugins></build></profile>"

        def remoteProfile = "<profile><id>remote</id><dependencies>" +
                pomCreator.buildSWTDependencyEntry(false) +
                "</dependencies><build><plugins>" +
                pomCreator.buildSureFirePluginEntry("<include>**/*Remote.java</include>") +
                "</plugins></build></profile>"

        pomCreator.pomProfiles = localProfile + remoteProfile

        // set directory to examples project
        pomCreator.pomDirectory = configAndObjects.workingDirectory

        // create pom
        pomCreator.createPom()
    }

    def buildBlackboxProject(Boolean useXvfb = false) {
        cleanUp()
        buildBlackBoxPom(useXvfb)
        copyTestClasses()
    }
}
