package org.evolvis.maven.plugins.remotetesting.projectbuilder

import org.evolvis.maven.plugins.remotetesting.util.BuildMachine

/**
 * helper class that creates poms
 */
class PomCreator {

    private AntBuilder ant = new AntBuilder()
    private BuildMachine buildMachine = new BuildMachine()
    private def config

    /**
     * specify the directory where the pom shall be created in
     */
    def pomDirectory

    /**
     * the groupID of the generated pom, defaulting to "org.evolvis.maven.plugins.remotetesting"
     */
    def pomGroupID = "org.evolvis.maven.plugins.remotetesting"

    /**
     * the artifactID of the generated pom, defaulting to "rt-example-project"
     */
    def pomArtifactId = "rt-example-project"

    /**
     * parent section of the generated pom, optional
     * eg. "<parent><groupId>foo</groupId><artifactId>bar</artifactId><version>0.1</version></parent>"
     */
    def pomParent

    /**
     * packaging of the generated pom, defaulting to "pom"
     */
    def pomPackaging = "pom"

    /**
     * content of the profiles section of the generated pom, defaulting to "<profile><id>remote</id></profile>"
     */
    def pomProfiles = "<profile><id>remote</id></profile>"

    private def rtPluginVersion
    private def rtRemoteHost
    private def rtRemoteUsername
    private def rtRemotePassword

    /**
     * executions content of the rt plugin configuration
     * eg. "<execution><id>remote testing</id><goals><goal>clean</goal></goals></execution>"
     * defaulting to "<execution><id>remote testing</id><goals><goal>clean</goal><goal>test</goal></goals><phase>integration-test</phase></execution>"
     */
    def rtPluginExecutions = "<execution><id>remote testing</id><goals><goal>clean</goal>" +
            "<goal>test</goal></goals><phase>integration-test</phase></execution>"

    /**
     * the profiles the rt plugin executes on the remote machine, comma separated, defaulting to "remote"
     */
    def rtPluginProfilesToExecute = "remote"

    PomCreator() {
        config = new ConfigSlurper().parse(this.class.getResource("/mvn-remote-test.config"))
        pomDirectory = config.test.workingDir
        rtPluginVersion = config.test.pluginVersion
        rtRemoteHost = config.test.remotemachine.host
        rtRemoteUsername = config.test.remotemachine.user
        rtRemotePassword = config.test.remotemachine.pass
    }

    /**
     * builds the rt plugin entry for the pom
     * @return
     */
    def buildRTPluginEntry(optionalPluginParameters) {
        def pluginEntry = "<plugin><groupId>org.evolvis.maven.plugins.remote-testing</groupId>" +
                "<artifactId>remote-testing-plugin</artifactId><version>${rtPluginVersion}</version>" +
                "<configuration><remoteFolder>/tmp/remoteTestingFakeProject</remoteFolder>" +
                "<testMachine>${rtRemoteHost}</testMachine><username>${rtRemoteUsername}</username>" +
                "<password>${rtRemotePassword}</password><profiles>${rtPluginProfilesToExecute}</profiles>"
        if (optionalPluginParameters) {
            pluginEntry += optionalPluginParameters
        }
        "${pluginEntry}</configuration><executions>${rtPluginExecutions}</executions></plugin>"
    }

    /**
     * builds an surefire plugin entry
     * @param includes , optional, eg "<include>**\/*_RT.java</include>"
     */
    def buildSureFirePluginEntry(includes) {
        def pluginEntry = "<plugin><groupId>org.apache.maven.plugins</groupId>" +
                "<artifactId>maven-surefire-plugin</artifactId><version>2.7.2</version><configuration>"
        if (includes) {
            pluginEntry += "<includes>${includes}</includes>"
        }
        "${pluginEntry}<systemPropertyVariables><property><name>swt.library.path</name><value>/usr/lib/jni</value>" +
                "</property></systemPropertyVariables></configuration></plugin>"
    }

    /**
     * build an swt plugin entry
     */
    def buildSWTDependencyEntry(Boolean apionly = true) {
        if (apionly) {
            return "<dependency><groupId>org.eclipse.swt</groupId><artifactId>swt-gtk-linux</artifactId>" +
                    "<classifier>api</classifier><version>3.5.1</version><scope>provided</scope></dependency>"
        }
        "<dependency><groupId>org.eclipse.swt</groupId><artifactId>swt-gtk-linux</artifactId><version>3.5</version>" +
                "<scope>system</scope><systemPath>/usr/share/java/swt.jar</systemPath></dependency>"
    }

    /**
     * creates a pom with a valid configuration by default, you may specify additional optional parameters
     * eg. "<remoteFolder>/home/user/my/folder</remoteFolder><foo>bar</foo>"
     * @param createMinimalRTPluginEntry , specifies if the rt plugin shall be inserted into poms build section (default: true)
     * @param optionalRTPluginConfigParameters (may be null, only optional params, required params get configured by default)
     * @return
     */
    def createPom(Boolean createMinimalRTPluginEntry = false, optionalRTPluginConfigParameters = null) {
        prepare()
        def pom = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><project xmlns=\"http://maven.apache.org/POM/4.0.0\"" +
                " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"" +
                " xsi:schemaLocation=\"http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd\">" +
                "<modelVersion>4.0.0</modelVersion><groupId>${pomGroupID}</groupId>" +
                "<artifactId>${pomArtifactId}</artifactId><name>${pomArtifactId}</name>" +
                "<version>${rtPluginVersion}</version><packaging>${pomPackaging}</packaging>"
        if (pomParent) {
            pom += pomParent
        }
        pom += "<profiles>${pomProfiles}</profiles>" +
                "<build><plugins><plugin><groupId>org.apache.maven.plugins</groupId>" +
                "<artifactId>maven-compiler-plugin</artifactId><version>2.3.2</version>" +
                "<configuration><source>1.6</source><target>1.6</target></configuration></plugin>"

        if (createMinimalRTPluginEntry) {
            pom += buildRTPluginEntry(optionalRTPluginConfigParameters)
        }

        pom += "</plugins></build><dependencies><dependency><groupId>junit</groupId>" +
                "<artifactId>junit</artifactId><version>4.8.2</version></dependency>" +
                "</dependencies><repositories><repository><id>evolvis-repository</id>" +
                "<name>evolvis Maven2 Repository</name><url>http://maven-repo.evolvis.org/releases</url>" +
                "<snapshots><enabled>false</enabled></snapshots></repository>" +
                "<repository><id>evolvis-snapshot-repository</id><name>evolvis Maven2 Snapshot Repository</name>" +
                "<url>http://maven-repo.evolvis.org/snapshots</url><snapshots><enabled>true</enabled></snapshots>" +
                "</repository></repositories><pluginRepositories><pluginRepository><id>evolvis</id>" +
                "<name>evolvis</name><url>http://maven-repo.evolvis.org/releases</url>" +
                "<snapshots><enabled>false</enabled></snapshots></pluginRepository>" +
                "<pluginRepository><id>evolvis-snapshot</id><name>evolvis Snapshot</name>" +
                "<url>http://maven-repo.evolvis.org/snapshots</url><snapshots><enabled>true</enabled></snapshots>" +
                "</pluginRepository></pluginRepositories></project>"

        File pomfile = new File("${pomDirectory}/pom.xml")
        pomfile.createNewFile()
        pomfile << pom
    }

    /**
     * attempts to remove the "pom.xml" in the workingdir and to create the workingdir to avoid errors
     * @return
     */
    private def prepare() {
        try {
            buildMachine.executeCommand("rm -f ${pomDirectory}/pom.xml")
        } catch (ex) {}
        try {
            ant.mkdir(dir: pomDirectory)
        } catch (ex) {}
    }
}