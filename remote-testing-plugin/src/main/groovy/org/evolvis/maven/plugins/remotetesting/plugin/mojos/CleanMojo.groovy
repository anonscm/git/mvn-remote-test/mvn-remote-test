package org.evolvis.maven.plugins.remotetesting.plugin.mojos

import org.codehaus.mojo.groovy.GroovyMojo
import org.evolvis.maven.plugins.remotetesting.util.BuildMachine
import org.evolvis.maven.plugins.remotetesting.util.RemoteMachine

/**
 * Tidies the test machine
 *
 * @goal clean
 * @phase clean
 */
class CleanMojo extends GroovyMojo {

    /**
     * the machine on which the tests shall be ran
     *
     * @parameter expression="${testMachine}"
     * @required
     */
    String testMachine

    /**
     * the username for the remote machine, defaulting to "maven"
     *
     * @parameter expression="${username}" default-value="maven"
     */
    String username

    /**
     * the password for the remote machine
     *
     * @parameter expression="${password}"
     */
    String password

    /**
     * the location of the ssh private key, defaulting to "/home/$user/.ssh/id_rsa"
     *
     * @parameter expression="${keyFile}"
     */
    String keyFile

    /**
     * the passphrase of the private key. defaulting to no passphrase
     *
     * @parameter expression="${passphrase}" default-value=" "
     */
    String keyPassphrase

    /**
     * the folder on the remote machine we use as working directory
     *
     * @parameter expression="${remoteFolder}" default-value="/tmp/remoteTesting"
     */
    String remoteFolder

    /**
     * method which is executed when the remote-testing-goal is invoked
     */
    void execute() {

        def remoteMachine = new RemoteMachine(testMachine, username, password, keyFile, keyPassphrase, log)
        def buildMachine = new BuildMachine(log)

        buildMachine.logLoud("[remote-testing-plugin] cleaning up remote test machine...")

        try {
            remoteMachine.executeCommand("rm -R -f ${remoteFolder}")
        } catch (ex) {
            log.warn("[remote-testing-plugin] could not clean up. ${ex.message}")
        }
    }


}
