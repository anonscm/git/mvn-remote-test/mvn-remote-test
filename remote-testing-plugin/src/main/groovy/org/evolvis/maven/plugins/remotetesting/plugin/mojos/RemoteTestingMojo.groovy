package org.evolvis.maven.plugins.remotetesting.plugin.mojos

import org.codehaus.mojo.groovy.GroovyMojo
import org.evolvis.maven.plugins.remotetesting.util.BuildMachine
import org.evolvis.maven.plugins.remotetesting.util.Pom
import org.evolvis.maven.plugins.remotetesting.util.RemoteMachine

/**
 * The "main" goal. Copies the project folder to the test machine, launches maven test and copies back the results.
 *
 * @goal test
 * @phase process-test-classes
 */
class RemoteTestingMojo extends GroovyMojo {

    /**
     * the goal that shall be invoked on the remote/testing machine
     *
     * @parameter expression="${goal}" default-value="test"
     */
    String goal

    /**
     * the pom file that shall be executed on the remote/testing machine
     *
     * @parameter expression="${pomfile}" default-value="pom.xml"
     */
    String pomfile

    /**
     * the profile that should be used on the remote/testing machine while running mvn test
     *
     * @parameter expression="${profiles}" default-value="remote"
     */
    String profiles

    /**
     * make sure this fits the default-value in "profiles" field comment
     */
    static String defaultProfile = "remote"

    /**
     * the machine on which the tests shall be ran
     *
     * @parameter expression="${testMachine}"
     * @required
     */
    String testMachine

    /**
     * the username for the remote machine, defaulting to "maven"
     *
     * @parameter expression="${username}" default-value="maven"
     */
    String username

    /**
     * the password for the remote machine
     *
     * @parameter expression="${password}"
     */
    String password

    /**
     * the location of the ssh private key, defaulting to "/home/$username/.ssh/id_rsa"
     *
     * @parameter expression="${keyFile}"
     */
    String keyFile

    /**
     * the passphrase of the private key. defaulting to no passphrase
     *
     * @parameter expression="${passphrase}" default-value=" "
     */
    String keyPassphrase

    /**
     * the display on the testmachine that shall be used for test execution
     *
     * @parameter expression="${display}" default-value="0"
     */
    String display

    /**
     * the folder on the remote machine we use as working directory
     *
     * @parameter expression="${remoteFolder}" default-value="/tmp/remoteTesting"
     */
    String remoteFolder

    /**
     * @parameter expression="${project}"
     * @required
     * @readonly
     */
    org.apache.maven.project.MavenProject project

    /**
     * the pom object
     */
    private Pom pom

    /**
     * validates that all given profiles exist in the pom
     */
    private def checkProfiles() {
        def configuredProfiles = pom.configuredProfiles();
        profiles.split(",").each {
            if (!configuredProfiles.contains(it.trim())) {
                def isDefaultString = ""
                if (it == defaultProfile) {
                    isDefaultString = "(default)"
                }
                fail("you need to specify at least one valid profile for profiles-parameter."
                        + " '${it}'${isDefaultString} not found.")
            }
        }
    }

    /**
     * tries to build a valid maven command from the parameters specified
     * also launches / kills xvfb after maven execution
     */
    private def buildMavenCommand() {
        String selectedDisplay
        String mavenCommand = new String()
        if (display.contains("xvfb")) {
            selectedDisplay = display.substring(display.lastIndexOf(":") + 1)
            mavenCommand += "Xvfb :${selectedDisplay} -ac -screen 0 1200x800x24 & sleep 3;"
        } else {
            selectedDisplay = display
        }
        mavenCommand += "export DISPLAY=:${selectedDisplay}; mvn -f ${remoteFolder}/${pom.pomFileName} -P ${profiles} ${goal}"
        if (display.contains("xvfb")) {
            mavenCommand += "; xvfb=`ps aux | grep \"Xvfb :${selectedDisplay}\" | grep -v grep | awk '{print \$2}'`; kill -2 \$xvfb"
        }
        mavenCommand
    }

    /**
     * method which is executed when the remote-testing-goal is invoked
     */
    void execute() {
        def config = new ConfigSlurper().parse(this.class.getResource("/mvn-remote-test.config"))
        pom = new Pom(pomfile)

        BuildMachine buildMachine = new BuildMachine(log)
        RemoteMachine remoteMachine = new RemoteMachine(testMachine, username, password, keyFile, keyPassphrase, log)

        def mavenOutputDir = project.build.directory
        def outputDirectory = "${mavenOutputDir}/${config.build.outputDirectory}"
        def projectArchiveName = config.build.projectArchiveName
        def resultsArchiveName = config.build.resultsArchiveName
        // cant take this from project.build.testOutputDirectory as it is not initialized before surefire is executed
        def mavenTestOutputDir = "${mavenOutputDir}/surefire-reports"

        buildMachine.logLoud("checking configuration...")
        try {
            checkProfiles()
        } catch (ex) {
            fail("Error with your configuration", ex)
        }

        buildMachine.logLoud("compressing project folder...")
        buildMachine.createZip("${pom.absolutePathToPom}/.", outputDirectory, projectArchiveName)

        buildMachine.logLoud("copying archive to test machine...")
        remoteMachine.storeFile("${outputDirectory}/${projectArchiveName}", remoteFolder)

        buildMachine.logLoud("uncompressing project folder on remote machine...")
        remoteMachine.executeCommand("unzip -o ${remoteFolder}/${projectArchiveName} -d ${remoteFolder}")

        buildMachine.logLoud("launching maven...")
        try {
            remoteMachine.executeCommand(buildMavenCommand())
        } catch (ex) {
            fail("Exception occured during remote maven execution (maybe tests failing?)", ex)
        }

        buildMachine.logLoud("remote execution of tests finished.")

        buildMachine.logLoud("compressing results on test machine...")
        def tarCommand = "zip -jr ${remoteFolder}/${resultsArchiveName} ${remoteFolder}/target/surefire-reports/"
        remoteMachine.executeCommand(tarCommand)

        buildMachine.logLoud("copying archive from test machine...")
        remoteMachine.deliverFile("${remoteFolder}/${resultsArchiveName}", "${outputDirectory}/")

        buildMachine.logLoud("uncompressing results...")
        buildMachine.extractZip("${outputDirectory}/${resultsArchiveName}", "${mavenTestOutputDir}/")

        buildMachine.logLoud("results fetched. they should now be available.")
    }
}
